#!/bin/bash

# steps:
# 1. build dev image
# 2. build package in container using dev image
# 3. build app image using package

set -e


APP_IMG="local/demo_project"
DEV_IMG="local/demo_project:dev"

# 1. build dev image
echo "Building dev image"
docker build -t $DEV_IMG -f .devcontainer/Dockerfile  ./.devcontainer

# 2. build package in container
rm -rf ./dist ./docker/dist
docker run --rm -v $(pwd):/workspace -w /workspace $DEV_IMG make build
cp -r ./dist ./docker/dist


# 3. build docker image
docker build -t $APP_IMG -f ./docker/Dockerfile  ./docker

# # run docker image
docker run --rm -it -p 8000:8000 $APP_IMG
