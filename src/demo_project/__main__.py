""" main entry point for demo_project. """

from demo_project import __version__


def main() -> None:
    """main entry point for demo_project."""
    print(f"Hello from demo_project version: {__version__}!")


if __name__ == "__main__":
    main()
